import { makerActionTypes } from "./cm-types";

const INITIAL_STATE = {
  makers: [],
  newCompanyModal: false,
  newMakerTitle: "",
};

const makerReducer = (state = INITIAL_STATE, action) => {
  if (action.type === makerActionTypes.FETCH_MAKER) {
    return {
      makers: action.payload,
    };
  } else if (action.type === makerActionTypes.MODAL_MENU) {
    return {
      ...state,
      newCompanyModal: !state.newCompanyModal,
      newMakerTitle: "",
    };
  } else if (action.type === makerActionTypes.NEW_MAKER_TITLE) {
    return {
      ...state,
      newMakerTitle: action.payload,
    };
  } else if (action.type === makerActionTypes.POST_NEW_MAKER) {
    return {
      ...state,
      makers: [...state.makers, action.payload],
      newCompanyModal: !state.newCompanyModal,
    };
  } else if (action.type === makerActionTypes.DELETE_MAKER) {
    return {
      ...state,
      makers: state.makers.filter((maker) => {
        return maker.id !== action.payload;
      }),
    };
  }
  return state;
};

export default makerReducer;
