import { makerActionTypes } from "./cm-types";
import * as makerUtils from "./cm-utils";

export const getMakers = () => {
  return async (dispatch) => {
    let data = await makerUtils.fetchMaker();
    dispatch({ type: makerActionTypes.FETCH_MAKER, payload: data });
  };
};

export const modalMenuHandle = () => {
  return {
    type: makerActionTypes.MODAL_MENU,
  };
};

export const newMakerTitle = (name) => {
  return {
    type: makerActionTypes.NEW_MAKER_TITLE,
    payload: name,
  };
};

export const postNewCompany = (name) => {
  if (name !== "") {
    return async (dispatch) => {
      let data = await makerUtils.postCompany(name);
      dispatch({ type: makerActionTypes.POST_NEW_MAKER, payload: data });
    };
  }
  return {
    type: "UNKNOWN",
  };
};

export const makerDelete = (id) => {
  return async (dispatch) => {
    let makerId = await makerUtils.deleteMaker(id);
    dispatch({ type: makerActionTypes.DELETE_MAKER, payload: makerId });
  };
};
