export const carActionTypes = {
  FETCH_CARS: "FETCH_CARS",
  CARS_MODAL_MENU: "CARS_MODAL_MENU",
  NEW_CAR_TITLE: "NEW_CAR_TITLE",
  NEW_CAR_PRICE: "NEW_CARD_PRICE",
  NEW_CAR_FUEL: "NEW_CAR_FUEL",
  NEW_CAR_TRANSMISSION: "NEW_CAR_TRANSMISSION",
  NEW_CAR_MILEAGE: "NEW_CAR_MILEAGE",
  POST_NEW_CAR: "POST_NEW_CAR",
  DELETE_CAR: "DELETE_CAR",
  UPDATE_CAR: "UPDATE_CAR",
  UPDATE_MODAL_MENU: "UPDATE_MODAL_MENU",
  CAR_INFO: "CAR_INFO",
};
