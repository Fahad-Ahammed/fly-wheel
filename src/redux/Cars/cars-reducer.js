import { carActionTypes } from "./cars-types";

const INITIAL_STATE = {
  cars: [],
  carsModalMenu: false,
  newCar: {
    name: "",
    price: "",
    fuel: "",
    transmission: "",
    mileage: "",
  },
  updateModalMenu: false,
  singleCarInfo: [],
};

const carReducer = (state = INITIAL_STATE, action) => {
  if (action.type === carActionTypes.FETCH_CARS) {
    return {
      cars: action.payload,
    };
  } else if (action.type === carActionTypes.CARS_MODAL_MENU) {
    console.log("reducer");
    return {
      ...state,
      carsModalMenu: !state.carsModalMenu,
      newCar: {
        name: "",
        price: "",
        fuel: "",
        transmission: "",
        mileage: "",
      },
    };
  } else if (action.type === carActionTypes.UPDATE_MODAL_MENU) {
    return {
      ...state,
      updateModalMenu: !state.updateModalMenu,
    };
  } else if (action.type === carActionTypes.NEW_CAR_TITLE) {
    return {
      ...state,
      newCar: { ...state.newCar, name: action.payload },
    };
  } else if (action.type === carActionTypes.NEW_CAR_PRICE) {
    return {
      ...state,
      newCar: { ...state.newCar, price: action.payload },
    };
  } else if (action.type === carActionTypes.NEW_CAR_FUEL) {
    return {
      ...state,
      newCar: { ...state.newCar, fuel: action.payload },
    };
  } else if (action.type === carActionTypes.NEW_CAR_TRANSMISSION) {
    return {
      ...state,
      newCar: { ...state.newCar, transmission: action.payload },
    };
  } else if (action.type === carActionTypes.NEW_CAR_MILEAGE) {
    return {
      ...state,
      newCar: { ...state.newCar, mileage: action.payload },
    };
  } else if (action.type === carActionTypes.POST_NEW_CAR) {
    return {
      ...state,
      cars: [...state.cars, action.payload],
      carsModalMenu: !state.carsModalMenu,
    };
  } else if (action.type === carActionTypes.DELETE_CAR) {
    return {
      ...state,
      cars: state.cars.filter((car) => {
        return car.id !== action.payload;
      }),
    };
  } else if (action.type === carActionTypes.UPDATE_CAR) {
    console.log("reducer");
    console.log(action.payload);
    return {
      ...state,
      cars: state.cars.map((car) => {
        if (car.id === action.payload[1]) {
          return { ...car, ...action.payload[0] };
        }
        return car;
      }),
      updateModalMenu: !state.updateModalMenu,
    };
  } else if (action.type === carActionTypes.CAR_INFO) {
    return {
      ...state,
      singleCarInfo: state.cars.filter((car) => car.id === action.payload),
    };
  }
  return state;
};

export default carReducer;
