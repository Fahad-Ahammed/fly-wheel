import { carActionTypes } from "./cars-types";
import * as carsUtils from "./cars.utils";

export const getCars = (id) => {
  return async (dispatch) => {
    let data = await carsUtils.fetchCars(id);
    dispatch({ type: carActionTypes.FETCH_CARS, payload: data });
  };
};

export const carsModalMenuHandle = () => {
  console.log("action");
  return {
    type: carActionTypes.CARS_MODAL_MENU,
  };
};

export const updateModal = () => {
  console.log("action");
  return {
    type: carActionTypes.UPDATE_MODAL_MENU,
  };
};

export const newCarTitle = (name) => {
  return {
    type: carActionTypes.NEW_CAR_TITLE,
    payload: name,
  };
};

export const newCarPrice = (price) => {
  return {
    type: carActionTypes.NEW_CAR_PRICE,
    payload: price,
  };
};
export const newCarFuel = (type) => {
  return {
    type: carActionTypes.NEW_CAR_FUEL,
    payload: type,
  };
};

export const newCarTransmission = (type) => {
  return {
    type: carActionTypes.NEW_CAR_TRANSMISSION,
    payload: type,
  };
};
export const newCarMileAge = (mileage) => {
  return {
    type: carActionTypes.NEW_CAR_MILEAGE,
    payload: mileage,
  };
};

export const postNewCar = (newCarDetails, makerId) => {
  if (newCarDetails.name === "") return { type: "UNKNOWN" };
  return async (dispatch) => {
    let data = await carsUtils.postCars(newCarDetails, makerId);
    dispatch({ type: carActionTypes.POST_NEW_CAR, payload: data });
  };
};

export const carDelete = (id) => {
  return async (dispatch) => {
    let carId = await carsUtils.deleteCar(id);
    dispatch({ type: carActionTypes.DELETE_CAR, payload: carId });
  };
};

export const carUpdate = (newCarDetails, id) => {
  return async (dispatch) => {
    await carsUtils.updateCar(newCarDetails, id);
    dispatch({ type: carActionTypes.UPDATE_CAR, payload: [newCarDetails, id] });
  };
};

export const carInformation = (id) => {
  return {
    type: carActionTypes.CAR_INFO,
    payload: id,
  };
};
