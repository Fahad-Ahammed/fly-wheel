import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getCars,
  carsModalMenuHandle,
  carDelete,
  updateModal,
  carInformation,
} from "../../redux/Cars/cars-action";
import "./Cars.css";
import NewCar from "./NewCar/NewCar";
import UpdateCar from "./UpdataCar/UpdateCar";

class Cars extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getCars(id);
  }
  render() {
    const [style, name] = this.props.location.state;
    const {
      cars,
      carsModalMenu,
      carsModalMenuHandle,
      carDelete,
      updateModalMenu,
      updateModal,
      carInformation,
    } = this.props;
    return (
      <div className="cars-page">
        <div style={style} className="cars-header"></div>
        <h1 className="car-company">{name.toUpperCase()}</h1>
        <button onClick={carsModalMenuHandle} className="car-add-btn">
          Add
        </button>
        {cars.map((car) => {
          let style = {
            backgroundImage: `url(${car.photo})`,
          };
          return (
            <div key={car.id} className="car-details">
              <div style={style} className="car-photo"></div>
              <div className="car-spec">
                <h1 className="car-detail-name">{`${name} ${car.name}`}</h1>
                <h1 className="car-price">{`₹${car.price} Lakh`}</h1>
                <div className="spec-container">
                  <div className="car-key-spec">
                    <i className="fa fa-tachometer" aria-hidden="true"></i>
                    <i className="fas fa-cogs"></i>
                    <i className="fas fa-gas-pump"></i>
                  </div>
                  <div className="car-key-spec">
                    <p>{`${car.mileage} kmpl`}</p>
                    <p>{car.transmission}</p>
                    <p>{car.fuel}</p>
                  </div>
                </div>
              </div>
              <div className="edit">
                <i
                  onClick={() => {
                    carInformation(car.id);
                    updateModal();
                  }}
                  className="fas fa-edit"
                ></i>
                <i
                  onClick={() => carDelete(car.id)}
                  className="fa fa-trash"
                  aria-hidden="true"
                ></i>
              </div>
              {updateModalMenu && <UpdateCar />}
            </div>
          );
        })}
        {carsModalMenu && <NewCar makerId={this.props.match.params.id} />}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.carReducer.cars,
    carsModalMenu: state.carReducer.carsModalMenu,
    updateModalMenu: state.carReducer.updateModalMenu,
  };
};

export default connect(mapStateToProps, {
  carDelete,
  carsModalMenuHandle,
  getCars,
  updateModal,
  carInformation,
})(Cars);
