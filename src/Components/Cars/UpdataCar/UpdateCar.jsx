import React from "react";
import { connect } from "react-redux";
import {
  updateModal,
  newCarFuel,
  newCarPrice,
  newCarTitle,
  newCarTransmission,
  newCarMileAge,
  carUpdate,
} from "../../../redux/Cars/cars-action";

import "./UpdateCar.css";

function UpdateCar({
  updateModal,
  newCarFuel,
  newCarPrice,
  newCarTitle,
  newCarTransmission,
  newCarMileAge,
  carUpdate,
  newCar,
  carInformation,
}) {
  const [carInfo] = carInformation;
  return (
    <div className="update-car-overlay">
      <div className="new-car-main">
        <form className="new-car-form" action="">
          <div className="input-group">
            <label className="input-names">Name</label>
            <input
              placeholder={carInfo.name}
              onChange={(event) => {
                newCarTitle(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Price</label>
            <input
              placeholder={`${carInfo.price} Lakh`}
              onChange={(event) => {
                newCarPrice(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Fuel</label>
            <input
              placeholder={carInfo.fuel}
              onChange={(event) => {
                newCarFuel(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Transmission</label>
            <input
              placeholder={carInfo.transmission}
              onChange={(event) => {
                newCarTransmission(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
          <div className="input-group">
            <label className="input-names">Mileage</label>
            <input
              placeholder={`${carInfo.mileage} kmpl`}
              onChange={(event) => {
                newCarMileAge(event.target.value);
              }}
              className="new-car-input"
              type="text"
            />
          </div>
        </form>
        <div className="new-car-btn">
          <button
            onClick={() => {
              carUpdate(newCar, carInfo.id);
            }}
            className="car-update"
          >
            Update
          </button>
          <button onClick={updateModal} className="new-car-cancel-btn">
            cancel
          </button>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    newCar: state.carReducer.newCar,
    carInformation: state.carReducer.singleCarInfo,
  };
};

export default connect(mapStateToProps, {
  updateModal,
  newCarFuel,
  newCarPrice,
  newCarTitle,
  newCarTransmission,
  newCarMileAge,
  carUpdate,
})(UpdateCar);
