import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";
export default function Header() {
  return (
    <div className="header">
      <Link to="/" className="link">
        <h1 className="main-header">Fly wheel</h1>
      </Link>
    </div>
  );
}
