import React from "react";
import { connect } from "react-redux";
import {
  newMakerTitle,
  postNewCompany,
  modalMenuHandle,
} from "../../../redux/CarMaker/cm-action";
import "./NewCompany.css";

function NewCompany({
  postNewCompany,
  newCompanyName,
  newMakerTitle,
  modalMenuHandle,
}) {
  return (
    <div className="modal-overlay ">
      <div className="modal-box">
        <div className="modal-title">
          <input
            className="modal-input"
            type="text"
            placeholder="Add company tilte"
            onChange={(event) => newMakerTitle(event.target.value)}
          />
          <span onClick={modalMenuHandle}>X</span>
        </div>
        <button
          onClick={() => postNewCompany(newCompanyName)}
          className=" close modal-btn "
        >
          Add company
        </button>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    newCompanyName: state.makerReducer.newMakerTitle,
  };
};

export default connect(mapStateToProps, {
  newMakerTitle,
  postNewCompany,
  modalMenuHandle,
})(NewCompany);
