import React from "react";
import { Route, Switch } from "react-router";
import Cars from "./Components/Cars/Cars";
import Header from "./Components/Header/Header";
import HomePage from "./Components/Home/HomePage";

export default function App() {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/:id" component={Cars} />
      </Switch>
    </>
  );
}
